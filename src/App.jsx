import React from "react";
import { BrowserRouter, Link, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';

// importamos los componentes de la aplicación (vistas)
import Inicio from './componentes/Inicio';
import Lista from './componentes/ListaAlumno';
import NuevoAlumno from './componentes/NuevoAlumno';
import ModificaContacto from './componentes/ModificaContacto';
import EliminaContacto from './componentes/EliminaContacto';
import P404 from './componentes/P404';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './CSS/App.css';

// importamos modelo
import AlumnoConstructor from './componentes/AlumnoConstructor';

// clase App 
export default class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      alumnos: [],
      ultimoAlumno: 0,
      cursos: [],
      ultimoCurso: 0,
      contactos: [],
      ultimoContacto: 0
    }

    this.guardaContacto = this.guardaContacto.bind(this);
    this.eliminaContacto = this.eliminaContacto.bind(this);
    this.cargaContactos = this.cargaContactos.bind(this);
    this.cargaContactos();
    this.saveData = this.saveData.bind (this);
    this.loadData = this.loadData.bind(this);
  }

  //extra guardado de datos
saveData(){
  var jsonData = JSON.stringify(this.state);
  localStorage.setItem("datagenda", jsonData);
}
         
//carga de datos
loadData(){
  var text = localStorage.getItem("datagenda");
  if (text){
      var obj = JSON.parse(text);
      this.setState(obj);
  }
}
  cargaContactos(){
    const contactosInicio = [
      new AlumnoConstructor (1, "indiana", "email","eddad", "hombre"),
      new AlumnoConstructor (2, "007", "james@bond.com","",""),
      new AlumnoConstructor (3, "spiderman", "peter@parker.com","",""),
    ];
    //atención! asignamos directamente state porque no es promise/asinc
    //de lo contrario lo haríamos mediante setState!!!
    this.state = {
      contactos: contactosInicio,
      ultimoContacto: 3
    };
  }
  deleteData(){
    window.localStorage.removeItem("datagenda");
   
  }

  
  guardaContacto(datos) {
    //solo si id=0 asignamos nuevo id y actualizamos ultimoContacto
    if (datos.id===0){
      datos.id = this.state.ultimoContacto+1;
      this.setState({ultimoContacto: datos.id});
    }
    // comprobaciones adicionales... email ya existe? datos llenos?
    // si todo ok creamos contacto y lo añadimos a la lista
    let nuevo = new AlumnoConstructor(datos.id, datos.nombre,datos.email, datos.edad, datos.sexo);
    // si contacto existe lo eliminamos!
    // esto es porque podemos llegar aquí desde nuevo contacto o desde modifica contacto
    let nuevaLista = this.state.contactos.filter( el => el.id!==nuevo.id);
    //añadimos elemento recien creado
    nuevaLista.push(nuevo);
    // finalmente actualizamos state
    this.setState({contactos: nuevaLista});
    
  }


  eliminaContacto(idEliminar) {
    //creamos lista a partir de state.contactos, sin el contacto con el id recibido
    let nuevaLista = this.state.contactos.filter( el => el.id!==idEliminar);
    //asignamos a contactos
    this.setState({contactos: nuevaLista});
    
  }

  render() {

    

    return (
      <BrowserRouter>
        <Container>
          <Row>
            <Col xs="3">
              <ul className="list-unstyled">
                <li> <Link to="/">Inicio</Link> </li>
                <li> <Link to="/listaAlumnos">Alumnos</Link> </li> 
                <li> <Link to="/nuevoAlumno">Añadir Alumno</Link> </li>
                <li> <Link to="/listaCursos">Cursos</Link> </li>
                <li> <Link to="/nuevoCurso">Añadir Curso</Link> </li>
              </ul>
            </Col>
            <Col xs="8">
              <Switch>
                <Route exact path="/" component={Inicio} />
                <Route path="/listaAlumnos" render={()=><Lista contactos={this.state.contactos} />} />
                <Route path="/nuevoAlumno" render={() => <NuevoAlumno guardaContacto={this.guardaContacto} />} />
                <Route path="/modificaAlumno/:idContacto" render={(props) => <ModificaContacto contactos={this.state.contactos} guardaContacto={this.guardaContacto} {...props} />} />
                <Route path="/eliminaAlumno/:idContacto" render={(props) => <EliminaContacto contactos={this.state.contactos} eliminaContacto={this.eliminaContacto} {...props} />}  />
                <Route component={P404} />
              </Switch>
            </Col>
          </Row>
          <Row>
            <Button onClick={this.loadData}>Cargar datos</Button>
            <Button onClick={this.saveData}>Guardar datos</Button>
            <Button onClick={this.deleteData}>Borrar datos</Button>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}
