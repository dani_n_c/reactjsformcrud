import React from "react";

import { Table} from 'reactstrap';
import { Link } from "react-router-dom";

export default class Lista extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {


        //para crear las filas hacemos un "map" de los contactos recibidos
        //previamente los ordenamos por id...
        let filas = this.props.contactos.sort((a,b) => a.id-b.id).map(contacto => {
            return (
                <tr key={contacto.id}>
                    <td>{contacto.id}</td>
                    <td>{contacto.nombre}</td>
                    <td>{contacto.email}</td>
                    <td>{contacto.edad}</td>
                    <td>{contacto.sexo}</td>
                    <td><Link to={"/modificaAlumno/" + contacto.id}>Editar</Link></td>
                    <td><Link to={"/eliminaAlumno/" + contacto.id}>Eliminar</Link></td>
                </tr>
            );
        })


        return (
            <Table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Edad</th>
                        <th>Genero</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        );
    }


}

