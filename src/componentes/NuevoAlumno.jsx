
import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';


class NuevoAlumno extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nombre: '',
            email: '',
            edad: '',
            sexo: '',
            volver: false
        };

        this.cambioInput = this.cambioInput.bind(this);
        this.submit = this.submit.bind(this);
    }

    //gestión genérica de cambio en campo input
    cambioInput(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    //método activado al enviar el form (submit)
    submit(e) {
        this.props.guardaContacto({
            nombre: this.state.nombre,
            email: this.state.email,
            edad: this.state.edad,
            sexo: this.state.sexo,
            id: 0
        });
        e.preventDefault();
        this.setState({ volver: true });
    }

    render() {
        //si se activa volver redirigimos a lista
        if (this.state.volver === true) {
            return <Redirect to='/listaAlumnos' />
        }

        return (

            <Form onSubmit={this.submit}>
                <Row>
                    <Col xs="6">
                        <FormGroup>
                            <Label for="nombreInput">Nombre</Label>
                            <Input type="text" 
                                name="nombre" 
                                id="nombreInput"
                                value={this.state.nombre}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="emailInput">Email</Label>
                            <Input type="text" name="email" id="emailInput"
                                value={this.state.email}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="edadInput">Edad</Label>
                            <Input type="text" name="edad" id="edadInput"
                                value={this.state.edad}
                                onChange={this.cambioInput} />
                        </FormGroup>
                        <FormGroup>
                            <Label for="sexoInput">Sexo</Label>
                            <Input type="text" name="sexo" id="sexoInput"
                                value={this.state.sexo}
                                onChange={this.cambioInput} />
                        </FormGroup>
                    </Col>
                </Row>


                <Row>
                    <Col>
                        <Button color="primary">Guardar</Button>
                    </Col>
                </Row>
            </Form>

        );
    }
}






export default NuevoAlumno;
